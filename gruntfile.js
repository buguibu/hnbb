
module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    meta: {
      file: "hnbb",
      tuktuk_file_name: "tuktuk",
      tuktuk_source_dir: "sources/tuktuk/sources",
      endpoint: "site"
    },
    source: {
      zepto_js_file: "sources/zepto.min.js",
      jquery_js_file: "sources/jquery-1.9.1.min.js",
      tuktuk:{
        coffee: ["<%= meta.tuktuk_source_dir %>/tuktuk.coffee", "<%= meta.tuktuk_source_dir %>/tuktuk.*.coffee"],
        stylus: ["<%= meta.tuktuk_source_dir %>/stylesheets/tuktuk.*.styl","<%= meta.tuktuk_source_dir %>/themes/theme.default.styl"],
        //theme: ["sources/themes/theme.default.styl"],
        icons: ["<%= meta.tuktuk_source_dir %>/componentes/lungo.icon/lungo.icon.css"]
      }
    },
    coffee: {
      engine: {
        files: {
          "<%= meta.endpoint %>/js/<%= meta.tuktuk_file_name %>.js": ["<%= source.tuktuk.coffee %>"]
        }
      }
    },
    stylus: {
      stylesheets: {
        options: {
          compress: true
        },
        files: {
          '<%= meta.endpoint %>/css/<%=meta.tuktuk_file_name%>.css': ['<%= source.tuktuk.stylus %>']
        }
      }
      /*,theme: {
        options: {
          compress: true
        },
        files: {
          '<%= meta.endpoint %>/<%=meta.file%>.theme.css': '<%= source.theme %>'
        }
      }*/
    },
    copy: {
      main: {
        files: [
          {
            src: ["<%= meta.tuktuk_source_dir %>/components/lungo.icon/lungo.icon.css"],
            dest: "<%= meta.endpoint %>/css/<%=meta.tuktuk_file_name%>.icons.css"
          },
          {
            src:["<%= source.zepto_js_file %>"],
            dest: "<%= meta.endpoint %>/js/zepto.js"
          },
          {
            src:["<%= source.zepto_js_file %>"],
            dest: "<%= meta.endpoint %>/js/jquery.js"
          }
        ]
      }
    },
    watch: {
      coffee: {
        files: ["<%= source.tuktuk.coffee %>"],
        tasks: ["coffee"]
      },
      stylus: {
        files: ["<%= source.tuktuk.stylus %>"],
        tasks: ["stylus"]
      }
    }
  });
  grunt.loadNpmTasks("grunt-contrib-coffee");
  grunt.loadNpmTasks("grunt-contrib-stylus");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-watch");
  
  grunt.registerTask("default", ["coffee", "stylus", "copy", "watch"]);
};
