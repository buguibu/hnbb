/*
I have choosen the "Thi Namespace Proxy" pattern for being a simple
and elegant pattern for simple applications. 
This pattern use the keyword "this" for doing the functions that i want public, 
other patterns do the same by returning a literal object with the choosen functions,
but using only the word this is cleaner and simple.
*/
var hnbb = {};
(function () {
	//I always try to work on strict mode
	'use strict';
	var conf, getNews, composeHnRequest, loadNews, handleAjaxError;
	/*
	For reading purposes and speed up future modifications, 
	i prefer to keep all the configuration params in a single object at the top of the file.
	*/
	conf = {
		APP_ID: '',
		GUID: 'a21d75c1e58e4c86ae6ea6369b2c4172',
		NEWEST_URL: '//hndroidapi.appspot.com/news/',
		COMMON_EXTRA_URL: 'format/json/page/?',
		N_NEWS_TO_SHOW: 10,
		JSON_ITEMS_ROOT: 'items',
		NEWS_CONTAINER_SELECTOR: '#articles_container'
	};
	//I prefer to put all the ugly string stuff in a separated method
	composeHnRequest = function (url) {
		return url +
			conf.COMMON_EXTRA_URL +
			'appid=' + conf.APP_ID +
			'&guid=' + conf.GUID;
	};
	loadNews = function (json) {
		if (!json[conf.JSON_ITEMS_ROOT].length) {
			return;
		}
		var i, html = '', news;
		for (i = 0; i < conf.N_NEWS_TO_SHOW; i = i + 1) {
			news = json[conf.JSON_ITEMS_ROOT][i];
			html =
				'<article class="row">' +
					'<div class="column_9 margin-bottom">' +
						'<h2>' +
							'<a href="#" class="text bold color theme">' + news.title + '</a>' +
						'</h2>' +
						'<h5>Written by <a href="#" class="text bold color theme">' + news.user + '</a> on ' + news.time + '</h5>' +
					'</div>' +
				'<p class="column_9"><a href=' + news.url + '>' + news.url + '</a></p>' +
				'</article>' +
				'</hr>';
			$(conf.NEWS_CONTAINER_SELECTOR).append(html);
		}
	};
	handleAjaxError = function (xhr, type) {
		//Use TukTuk API for showing a window Modal
		TukTuk.Modal.show('error_modal');
	};
	getNews = function () {
		$.ajax({
			type: 'GET',
			dataType: 'jsonp', //For avoid Access Cross Origin problems
			url: composeHnRequest(conf.NEWEST_URL),
			success: loadNews,
			error: handleAjaxError
		});
	};
	this.init = function () {
		getNews();
	};
}).apply(hnbb);
/*
I could do the next in a separate file, but for performance reasons
i prefer to keep this code at the end of this file for trying to use 
at least files as possible.
*/
$(document).ready(function () {
	'use strict';
	hnbb.init();
});